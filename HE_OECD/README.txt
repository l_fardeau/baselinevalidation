The SHA export comes from this website: http://dotstat.oecd.org/?lang=en

-> First restrict costs to to governmental/compulsory schemes, then select these components:
	- Curative/rehabilitative care (everything) 
	- Preventative care
	- Medical goods

-> Select current prices (all & per capita)

-> Export as .csv

