import matplotlib.pyplot as plt
import matplotlib.gridspec as grd
import seaborn as sns

from .validation_tables import ValidationTable

class Tools:
    @staticmethod
    def lines(axis, baseline, inputs):

        axis.plot(baseline['year'], baseline['mean'], color='xkcd:plum')
        axis.plot(inputs['year'], inputs['all'], color='salmon')

        axis.plot(baseline['year'], baseline['mean'] + baseline['c95'], color='plum', alpha=0.6)
        axis.plot(baseline['year'], baseline['mean'] - baseline['c95'], color='plum', alpha=0.6)

        axis.set_xlabel('Time')
        return axis

    @staticmethod
    def pyramid(axis1, axis2, baseline, inputs):

        sns.barplot(x='mean', y='age-group', color='steelblue', ax=axis1,
                    order=['65+', '31-65', '16-30', '0-15'], ci=None, label='males, OECD SPHeP outputs',
                    data=baseline[(baseline.gender == 'Male')], alpha=0.6)
        sns.pointplot(x='male', y='age_group', color='navy', ax=axis1,
                      order=['65+', '31-65', '16-30', '0-15'], ci=None, label='males, Inputs',
                      data=inputs, orient='h', join=False)

        # Bar chart for female
        sns.barplot(x='mean', y='age-group', color='darkslateblue', ax=axis2,
                    order=['65+', '31-65', '16-30', '0-15'], ci=None, label='females, OECD SPHeP outputs',
                    data=baseline[(baseline.gender == 'Female')], alpha=0.6)
        sns.pointplot(x='female', y='age_group', color='midnightblue', ax=axis2,
                      order=['65+', '31-65', '16-30', '0-15'], ci=None, label='females, Inputs',
                      data=inputs, orient='h', join=False)

        # Use matplotlib function to invert the first chart
        axis1.invert_xaxis()
        # Use matplotlib function to show tick labels in the middle
        axis1.yaxis.tick_right()
        axis2.get_yaxis().set_visible(False)

        return axis1, axis2

    @staticmethod
    def pyramid2(axis1, axis2, varname1, varname2, baseline1, baseline2, inputs1, inputs2):

        sns.barplot(x='mean', y='age-group', color='lightsteelblue', ax=axis1,
                    order=['65+', '31-65', '16-30', '0-15'], ci=None, label=varname1 + ' OECD SPHeP outputs',
                    data=baseline1[(baseline1.gender == 'Male')], alpha=0.5)
        sns.barplot(x='mean', y='age-group', color='steelblue', ax=axis1,
                    order=['65+', '31-65', '16-30', '0-15'], ci=None, label=varname2 + ' OECD SPHeP outputs',
                    data=baseline2[(baseline2.gender == 'Male')], alpha=0.7)

        sns.pointplot(x='male', y='age_group', color='darkblue', ax=axis1,
                      order=['65+', '31-65', '16-30', '0-15'], ci=None, label=varname1 + ' Inputs',
                      data=inputs1, orient='h', join=False)
        sns.pointplot(x='male', y='age_group', color='darkblue', ax=axis1, markers='x',
                      order=['65+', '31-65', '16-30', '0-15'], ci=None, label=varname2 + ' Inputs',
                      data=inputs2, orient='h', join=False)

        # Bar chart for female
        sns.barplot(x='mean', y='age-group', color='mediumslateblue', ax=axis2,
                    order=['65+', '31-65', '16-30', '0-15'], ci=None, label=varname1 + ' OECD SPHeP outputs',
                    data=baseline1[(baseline1.gender == 'Female')], alpha=0.5)
        sns.barplot(x='mean', y='age-group', color='darkslateblue', ax=axis2,
                    order=['65+', '31-65', '16-30', '0-15'], ci=None, label=varname2 + ' OECD SPHeP outputs',
                    data=baseline2[(baseline2.gender == 'Female')], alpha=0.7)

        sns.pointplot(x='female', y='age_group', color='indigo', ax=axis2,
                      order=['65+', '31-65', '16-30', '0-15'], ci=None, label=varname1 + ' Inputs',
                      data=inputs1, orient='h', join=False)
        sns.pointplot(x='female', y='age_group', color='indigo', ax=axis2, markers='x',
                      order=['65+', '31-65', '16-30', '0-15'], ci=None, label=varname2 + ' Inputs',
                      data=inputs2, orient='h', join=False)

        # Use matplotlib function to invert the first chart
        axis1.invert_xaxis()
        # Use matplotlib function to show tick labels in the middle
        axis1.yaxis.tick_right()

        axis2.get_yaxis().set_visible(False)
        return axis1, axis2

    @staticmethod
    def process_unit(unit, baseline, inputs, graph_type):

        if graph_type == 'Trend':
            baseline['mean'] /= unit
            baseline['c95'] /= unit
            inputs['all'] /= unit

        elif graph_type == 'Pyramid':
            baseline['mean'] /= unit
            inputs['male'] /= unit
            inputs['female'] /= unit

        else:
            raise Exception("Graph type missing or incorrectly specified : should be either 'Trend' or 'Pyramid'")

        return baseline, inputs


class GraphTemplates(ValidationTable, Tools):
    def __init__(self, countries, inputs_path):
        self._countries = countries
        self._table = ValidationTable(countries, inputs_path)

    def trend_over_time(self, baseline, inputs, axis_name, countries, unit=None, in_percent=False):

        if (unit is not None) and in_percent:
            raise Exception('Options unit and in_percent cannot be used together')

        fig = plt.figure(figsize=(15, 40))
        gs = grd.GridSpec(14, 2, figure=fig, hspace=0.5)

        baseline = baseline.copy()
        inputs = inputs.copy()

        if in_percent:
            baseline['mean'] *= 100
            baseline['c95'] *= 100
            inputs['all'] *= 100

        Tab = self._table.table_validation(baseline, inputs, countries)

        for c, i in zip(countries, range(len(countries))):
            # data
            baseline_ctr = baseline[(baseline.country == c)].copy()
            inputs_ctr = inputs[(inputs.country == c)].copy()
            # subplots
            gs_i = grd.GridSpecFromSubplotSpec(1, 3, subplot_spec=gs[i])
            ax_i = plt.Subplot(fig, gs_i[:, 1:3])

            # labels
            if unit is not None:
                baseline_ctr, inputs_ctr = Tools.process_unit(unit, baseline_ctr, inputs_ctr, graph_type='Trend')
                ax_i.set_ylabel(axis_name + ', ' + str(unit))
            elif in_percent:
                ax_i.set_ylabel(axis_name + ', %')
            else:
                ax_i.set_ylabel(axis_name)
            ax_i = Tools.lines(ax_i, baseline_ctr, inputs_ctr)
            ax_i.set_title(c)

            # additional statistics
            textstr = ValidationTable.display_stats_simple(axis=i, table=Tab, graph_type='trend', in_percent=in_percent)
            ax_i.text(-0.8, 0.1, textstr, fontsize=11, transform=ax_i.transAxes)

            if i == len(countries)-1:
                ax_i.legend(['OECD SPHeP outputs', 'Inputs'], loc=9, bbox_to_anchor=(-0.8, -0.4), fontsize=12)
            fig.add_subplot(ax_i)

        return fig

    def age_pyramid(self, baseline, inputs, axis_name, unit, countries):

        fig = plt.figure(figsize=(15, 40))
        gs = grd.GridSpec(14, 2, figure=fig, hspace=0.5)

        Tab = self._table.average_diff(baseline, inputs, countries)

        for c, i in zip(countries, range(0, len(countries))):

            # data
            baseline_ctr = baseline[(baseline.country == c)].copy()
            inputs_ctr = inputs[(inputs.country == c)].copy()

            # Subplots
            gs_i = grd.GridSpecFromSubplotSpec(1, 3, subplot_spec=gs[i], wspace=0.4)
            ax1_i = plt.Subplot(fig, gs_i[:, 1])
            ax2_i = plt.Subplot(fig, gs_i[:, 2])

            if unit is not None:
                baseline_ctr, inputs_ctr = Tools.process_unit(unit, baseline_ctr, inputs_ctr, graph_type='Pyramid')
                ax1_i.set(xlabel='Male ' + axis_name + ', ' + str(unit), ylabel='Age Group')
                ax2_i.set(xlabel='Female ' + axis_name + ', ' + str(unit), ylabel='Age Group')
            else:
                ax1_i.set(xlabel='Male ' + axis_name, ylabel='Age Group')
                ax2_i.set(xlabel='Female ' + axis_name, ylabel='Age Group')

            # Seaborn plots
            ax1_i, ax2_i = Tools.pyramid(ax1_i, ax2_i, baseline_ctr, inputs_ctr)
            ax1_i.set_title(c)

            # Adding the subplots
            fig.add_subplot(ax1_i)
            fig.add_subplot(ax2_i)

            # Legend and text
            if i == len(countries)-1:
                ax1_i.legend(['male, OECD SPHeP outputs', 'male, Inputs'], loc=10, bbox_to_anchor=(-3.5, -1), fontsize=12)
                ax2_i.legend(['female, OECD SPHeP outputs', 'female, Inputs'], loc=10, bbox_to_anchor=(-2, -1), fontsize=12)

            textstr = ValidationTable.display_stats_simple(axis=i, table=Tab, graph_type='pyramid')
            ax1_i.text(-1.7, 0.5, textstr, fontsize=12, transform=ax1_i.transAxes)

        return fig


    def age_pyramid2(self, baseline1, baseline2, inputs1, inputs2, varname1, varname2, axis_name, countries, unit, year_range):
        """
        Age Pyramid with 2 types of inputs. Actually (1) is supposed to be a subset of (2).
        """

        fig = plt.figure(figsize=(15, 40))
        gs = grd.GridSpec(14, 2, figure=fig, hspace=0.5)

        Tab1 = self._table.average_diff(baseline1, inputs1, countries, year_range, prevalence=True)
        Tab2 = self._table.average_diff(baseline2, inputs2, countries, year_range, prevalence=True)

        for c, i in zip(countries, range(0, len(countries))):

            # Data
            baseline1_ctr = baseline1[(baseline1.country == c)].copy()
            baseline2_ctr = baseline2[(baseline2.country == c)].copy()
            inputs1_ctr = inputs1[(inputs1.country == c)].copy()
            inputs2_ctr = inputs2[(inputs2.country == c)].copy()

            # Subplots
            gs_i = grd.GridSpecFromSubplotSpec(1, 3, subplot_spec=gs[i], wspace=0.4)
            ax1_i = plt.Subplot(fig, gs_i[:, 1])
            ax2_i = plt.Subplot(fig, gs_i[:, 2])

            if unit is not None:
                baseline1_ctr, inputs1_ctr = Tools.process_unit(unit, baseline1_ctr, inputs1_ctr, graph_type='Pyramid')
                baseline2_ctr, inputs2_ctr = Tools.process_unit(unit, baseline2_ctr, inputs2_ctr, graph_type='Pyramid')
                ax1_i.set(xlabel='Male ' + axis_name + ', ' + str(unit), ylabel='Age Group')
                ax2_i.set(xlabel='Female ' + axis_name + ', ' + str(unit), ylabel='Age Group')
            else:
                ax1_i.set(xlabel='Male ' + axis_name, ylabel='Age Group')
                ax2_i.set(xlabel='Female ' + axis_name, ylabel='Age Group')

            # Seaborn plots
            ax1_i, ax2_i = Tools.pyramid2(ax1_i, ax2_i, varname1, varname2, baseline1_ctr, baseline2_ctr, inputs1_ctr, inputs2_ctr)
            # Use matplotlib functions to label the axes and titles
            ax1_i.set_title(c)

            # Adding the subplots
            fig.add_subplot(ax1_i)
            fig.add_subplot(ax2_i)

            # Legend and text
                #Legend
            if i == len(countries)-1:
                ax1_i.legend([varname1 + ' Inputs', varname2 + ' Inputs', varname1 + ' OECD SPHeP outputs',
                              varname2 + ' OECD SPHeP outputs'], loc=10, bbox_to_anchor=(-3.5,  -1.2), fontsize=11)
                ax2_i.legend([varname1 + ' Inputs', varname2 + ' Inputs', varname1 + ' OECD SPHeP outputs',
                              varname2 + ' OECD SPHeP outputs'], loc=10, bbox_to_anchor=(-2, -1.2), fontsize=11)

                # Text
            textstr = ValidationTable.display_stats_double(axis=i, table1=Tab1, table2=Tab2, varname1=varname1, varname2=varname2)
            ax1_i.text(-1.7, -0.1, textstr, fontsize=12, transform=ax1_i.transAxes)

        return fig