from .graph_templates import GraphTemplates
import pandas as pd
from tables import InputsLoader, BaselineLoader

from local_settings import COUNTRIES_HE

class GraphByCategory(GraphTemplates, InputsLoader, BaselineLoader):
    def __init__(self, countries, inputs_path, baseline_path):
        self._countries = countries
        self._graph = GraphTemplates(countries, inputs_path)
        self._inputs = InputsLoader(inputs_path, countries)
        self._baseline = BaselineLoader(baseline_path)
        self._risk_factors = self._baseline.risk_factors()
        self._CVD_prevalence = self._baseline.CVD_prevalence()
        self._CVD_incidence = self._baseline.CVD_incidence()

    def graphs_population(self):

        baseline, baselineAG = self._baseline.population()
        inputs = self._inputs.aggregate_pop()
        inputsAG = self._inputs.pop_table_by_AG().groupby(['country', 'age_group'])['female', 'male'].mean().reset_index()

        Gr1 = self._graph.trend_over_time(baseline, inputs, countries=self._countries, axis_name='Pop', unit=1e6)
        Gr2 = self._graph.age_pyramid(baselineAG, inputsAG, countries=self._countries, axis_name='Pop', unit=1e6)
        return Gr1, Gr2

    def employment_rate(self):

        baseline = self._baseline.employment().groupby(['gender', 'country', 'age-group'])['mean', 'std_error', 'c95'].mean().reset_index()
        input = self._inputs.employment_table().groupby(['country', 'age_group'])['female', 'male'].mean().reset_index()

        graph = self._graph.age_pyramid(baseline, input, countries=self._countries, axis_name='ER', unit=None)
        return graph

    def graphs_alcohol(self):

        baseline = self._risk_factors.groupby(['gender', 'country', 'age-group', 'risk_factor'])['mean', 'std_error', 'c95'].mean().reset_index()
        baseline_drink = baseline.loc[baseline.risk_factor == 'Drinker']
        baseline_binge = baseline.loc[baseline.risk_factor == 'Binge drinker']

        inputs = self._inputs.alcohol_prevalence()
        inputs_drink = inputs[['country', 'age_group', 'male_current', 'female_current']].rename(columns={'male_current': 'male', 'female_current': 'female'})
        inputs_binge = inputs[['country', 'age_group', 'male_binge', 'female_binge']].rename(columns={'male_binge': 'male', 'female_binge': 'female'})

        Graph = self._graph.age_pyramid2(baseline_drink, baseline_binge, inputs_drink, inputs_binge, 'drinking', 'binge drink.', axis_name='Alcohol prev.', unit=1e-2, countries=self._countries, year_range=range(2005,2016))
        return Graph

    def graphs_BMI(self):

        baseline = self._risk_factors.groupby(['gender', 'country', 'age-group', 'risk_factor'])['mean', 'std_error', 'c95'].mean().reset_index()
        baseline_overweight = baseline.loc[baseline.risk_factor == 'Overweight'].reset_index()
        baseline_obese = baseline.loc[baseline.risk_factor == 'Obese'].reset_index()
        baseline_overweight['mean'] += baseline_obese['mean']

        inputs = self._inputs.obesity_overweight()
        inputs_overweight = inputs[['country', 'age_group', 'male_overweight', 'female_overweight']].rename(columns={'male_overweight': 'male', 'female_overweight': 'female'}).reset_index()
        inputs_obese = inputs[['country', 'age_group', 'male_obesity', 'female_obesity']].rename(columns={'male_obesity': 'male', 'female_obesity': 'female'}).reset_index()
        inputs_overweight['male'] += inputs_obese['male']
        inputs_overweight['female'] += inputs_obese['female']

        graph = self._graph.age_pyramid2(baseline_overweight, baseline_obese, inputs_overweight, inputs_obese, 'overw.', 'obesity', axis_name='High BMI prev.', unit=1e-2, countries=self._countries, year_range=range(2005, 2015))
        return graph

    def graphs_cancer(self):

        baseline = self._baseline.cancer()
        inputs = self._inputs.cancer_incidence()

        graph = self._graph.trend_over_time(baseline, inputs, axis_name='Cancers inc.', countries=self._countries, unit=1000)
        return graph

    def graphs_diabetes(self):

        baseline_incidence = self._baseline.diabetes_incidence()
        baseline_prevalence = self._baseline.diabetes_prevalence()
        inputs_incidence, inputs_prevalence = self._inputs.disease_table('Diabetes')

        Gr1 = self._graph.trend_over_time(baseline_incidence, inputs_incidence, axis_name='Diabetes inc.', countries=self._countries, unit=1000)
        Gr2 = self._graph.trend_over_time(baseline_prevalence, inputs_prevalence, axis_name='Diabetes prev.', countries=self._countries, in_percent=True)
        return Gr1, Gr2

    def graphs_IHD_MI(self):

        baseline_incidence = self._CVD_incidence.loc[self._CVD_incidence.disease == 'IHD_MI']
        baseline_prevalence = self._CVD_prevalence.loc[self._CVD_prevalence.disease == 'IHD_MI']
        inputs_incidence, inputs_prevalence = self._inputs.disease_table('IHD_MI')

        Gr1 = self._graph.trend_over_time(baseline_incidence, inputs_incidence, axis_name='IHD MI inc.', countries=self._countries, unit=1000)
        Gr2 = self._graph.trend_over_time(baseline_prevalence, inputs_prevalence, axis_name='IHD MI prev.', countries=self._countries, in_percent=True)
        return Gr1, Gr2

    def graphs_ischemic_stroke(self):

        baseline_incidence = self._CVD_incidence.loc[self._CVD_incidence.disease == 'Ischemic_Stroke'].copy()
        baseline_prevalence = self._CVD_prevalence.loc[self._CVD_prevalence.disease == 'Ischemic_Stroke'].copy()
        inputs_incidence, inputs_prevalence = self._inputs.disease_table('Ischemic_Stroke')

        Gr1 = self._graph.trend_over_time(baseline_incidence, inputs_incidence, axis_name='Ischemic Stroke inc.', countries=self._countries, unit=1000)
        Gr2 = self._graph.trend_over_time(baseline_prevalence, inputs_prevalence, axis_name='Isch. Stroke prev.', countries=self._countries, in_percent=True)
        return Gr1, Gr2

    def graph_HE(self):

        baseline = self._baseline.health_expenditure()
        inputs = self._inputs.health_expenditure()
        # graph = self._graph.trend_over_time(baseline, inputs, axis_name='Health Expenditure', countries=COUNTRIES_HE, unit=None)
        # return graph

        baseline = baseline.loc[(baseline.year == 2014) & (baseline['country'].isin(inputs['country'].tolist()))
                                ].sort_values(['country']).reset_index(drop=True)
        inputs = inputs.loc[(inputs.year == 2014) & (inputs['country'].isin(baseline['country'].tolist()))
                            ].sort_values(['country']).reset_index(drop=True)

        table = pd.DataFrame(columns={'country', 'baseline', 'OECD data'})
        table[['country', 'baseline']] = baseline[['country', 'mean']]
        table['OECD data'] = inputs['all']

        return table.reindex(columns=['country', 'baseline', 'OECD data'])

