import os

from graphs import GraphByCategory
from tables import MortalityCheck
from local_settings import GRAPHS, COUNTRIES, BASELINE, INPUT_DATA_DB, MORT_TABLES


class GraphsValidation(GraphByCategory, MortalityCheck):
    def __init__(self, countries, inputs_path, baseline_path):
        self._output = GRAPHS
        self._output_mort = MORT_TABLES
        self._inputs_path = inputs_path
        self._graphs = GraphByCategory(countries, inputs_path, baseline_path)
        self._mortality = MortalityCheck(inputs_path, countries)

    def create_graphs(self):

        # pop1, pop2 = self._graphs.graphs_population()
        # pop1.savefig(self._output + 'population_over_time.pdf')
        # pop2.savefig(self._output + 'population_by_age_groups.pdf')
        #
        # cancers = self._graphs.graphs_cancer()
        # cancers.savefig(self._output + 'cancer_incidence.pdf')
        #
        # empl = self._graphs.employment_rate()
        # empl.savefig(self._output + 'employment_rate.pdf')
        #
        # alcohol = self._graphs.graphs_alcohol()
        # alcohol.savefig(self._output + 'alcohol.pdf')
        #
        # BMI = self._graphs.graphs_BMI()
        # BMI.savefig(self._output + 'BMI.pdf')
        #
        # diab_inc, diab_prev = self._graphs.graphs_diabetes()
        # diab_inc.savefig(self._output + 'diabetes_incidence.pdf')
        # diab_prev.savefig(self._output + 'diabetes_prevalence.pdf')
        #
        # IHD_MI_inc, IHD_MI_prev = self._graphs.graphs_IHD_MI()
        # IHD_MI_inc.savefig(self._output + 'IHD_MI_incidence.pdf')
        # IHD_MI_prev.savefig(self._output + 'IHD_MI_prevalence.pdf')
        #
        # ischemic_stroke_inc, ischemic_stroke_prev = self._graphs.graphs_ischemic_stroke()
        # ischemic_stroke_inc.savefig(self._output + 'ischemic_stroke_incidence.pdf')
        # ischemic_stroke_prev.savefig(self._output + 'ischemic_stroke_prevalence.pdf')

        HEgraph = self._graphs.graph_HE()
        HEgraph.to_excel(self._output + 'HE.xlsx', index=False)

    def get_mortality(self):
        mortality, diseases_mortality = self._mortality.final_table()

        mortality.to_csv(self._output_mort+'mortality.csv', index=False)
        diseases_mortality.to_csv(self._output_mort+'diseases_mortality.csv', index=False)


def main():

    if not os.path.exists(GRAPHS):
        os.makedirs(GRAPHS)
        print('OUTPUTS directory created')

    graphs = GraphsValidation(countries=COUNTRIES, inputs_path=INPUT_DATA_DB, baseline_path=BASELINE)

    graphs.create_graphs()

main()

def main_mort():

    if not os.path.exists(MORT_TABLES):
        os.makedirs(MORT_TABLES)
        print('MORTALITY directory created')

    graphs = GraphsValidation(countries=COUNTRIES, inputs_path=INPUT_DATA_DB, baseline_path=BASELINE)

    graphs.get_mortality()

# main_mort()

