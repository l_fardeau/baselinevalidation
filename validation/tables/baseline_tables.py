import pandas as pd

class BaselineLoader:
    def __init__(self, baseline_path):
        self._baseline = baseline_path

    def population(self):
        pop = pd.read_excel(self._baseline, sheet_name='Population')
        popAG = pd.read_excel(self._baseline, sheet_name='Population_by_age_gender')
        popAG = popAG.groupby(['country', 'age-group', 'gender'])['mean'].mean().reset_index()
        return pop, popAG

    def employment(self):
        empl = pd.read_excel(self._baseline, sheet_name='BaselineER')
        return empl

    def risk_factors(self):
        riskfactors = pd.read_excel(self._baseline, sheet_name='RiskFactorPrevalence')
        return riskfactors

    def cancer(self):
        cancer = pd.read_excel(self._baseline, sheet_name='CancerIncidence')
        return cancer

    def diabetes_incidence(self):
        diab = pd.read_excel(self._baseline, sheet_name='DiabetesIncidence')
        return diab

    def diabetes_prevalence(self):
        prevalence = pd.read_excel(self._baseline, sheet_name='DiabetesPrevalence')
        return prevalence

    def CVD_incidence(self):
        incidence = pd.read_excel(self._baseline, sheet_name='CVDsIncidence')
        return incidence

    def CVD_prevalence(self):
        incidence = pd.read_excel(self._baseline, sheet_name='CVDsPrevalence')
        return incidence

    def health_expenditure(self):
        HE = pd.read_excel(self._baseline, sheet_name='HE')
        HE = HE.loc[HE['year'].isin(range(2005,2017))]
        return HE
