import pandas as pd
import numpy as np
import sqlite3

from local_settings import HEALTH_EXPENDITURE, CURRENCIES_PATH


AGE_GROUPS = {'0-15': range(0, 16),
              '16-30': range(16, 31),
              '31-65': range(31, 66),
              '65+': range(65, 111)}


CURRENCIES = {'CZE': 'Czech koruna',
              'DNK': 'Danish krone',
              'HUN': 'Hungarian forint',
              'POL': 'Polish zloty',
              'SWE': 'Swedish krona'}

CURRENCIES_EZ = {'LVA': 'Latvian lats',
                 'LTU': 'Lithuanian litas'}

CANCERS = 'BreastCancer Cirrhosis ColorectumCancer EsophagealCancer LungCancer'.split()

YEARS1 = range(1990, 2051)
YEARS2 = range(2005, 2018)
YEARS3 = range(2005, 2051)
YEARS4 = range(2005, 2015)
YEARS5 = range(2005, 2016)
YEARS_DISEASE = range(2005, 2017)


class Currencies:

    def __init__(self, path):
        self._currencies = CURRENCIES
        self._path = path

    def get_yearly_exchange_rates(self):
        er_data = pd.read_csv(self._path + 'ert_bil_eur_a_1_Data.csv')
        er_data['country'] = ''

        for country, currency in self._currencies.items():
            er_data.loc[er_data.CURRENCY == currency, 'country'] = country

        er_data = er_data.loc[(er_data.country != '') & (er_data.STATINFO == 'Average')
                              ][['TIME', 'country', 'Value']].rename(columns={'TIME': 'year',
                                                                              'Value': 'exchange_rate'}).reset_index()

        er_data['exchange_rate'] = er_data['exchange_rate'].apply(lambda x: float(x))
        return er_data

    def euro_converter(self, data):
        er = Currencies.get_yearly_exchange_rates(self)

        for country in self._currencies.keys():
            data.loc[data.country == country,
                     'all'] = np.divide(data.loc[data.country == country, 'all'].reset_index(drop=True),
                              er.loc[er.country == country, 'exchange_rate'].reset_index(drop=True)).tolist()

        return data


class InputsLoader(Currencies):

    def __init__(self, inputs_path, countries):
        self._converter = Currencies(CURRENCIES_PATH)
        self._inputs = inputs_path
        self._age_groups = AGE_GROUPS
        self._countries = countries
        self._population = self.population()
        self._tot_pop = self.aggregate_pop()
        self._popAG = self.pop_table_by_AG()
        self._cancers = CANCERS
        self._HE = HEALTH_EXPENDITURE

    def get_table_from_db(self, country, table):
        connection = sqlite3.connect(self._inputs + country + '.db')

        cursor = connection.cursor()
        cursor.execute("PRAGMA TABLE_INFO({})".format(table))
        nn = cursor.fetchall()
        names = pd.DataFrame(nn)[1].tolist()

        cursor.execute("SELECT * FROM {};".format(table))
        dd = cursor.fetchall()
        data = pd.DataFrame(dd, columns=names)

        connection.close()

        return data

    def population(self):
        pop_all = pd.DataFrame(columns=['country', 'year', 'age', 'female', 'male'])

        for country in self._countries:
            population = InputsLoader.get_table_from_db(self, country, 'Population')
            population['country'] = country
            pop_all = pop_all.append(population, ignore_index=True)

        pop_all = pop_all.loc[pop_all['year'].isin(YEARS1)].reset_index()

        return pop_all

    def aggregate_pop(self):

        population_agg = self._population.groupby(['country', 'year'])['female', 'male'].sum().reset_index()
        population_agg['all'] = population_agg.male + population_agg.female

        return population_agg

    def group_age_groups_add_ctr(self, table, country, grouped='female male'.split()):

        table['age_group'] = ''
        for age_group, age_range in self._age_groups.items():
            table.loc[table['age'].isin(age_range), 'age_group'] = age_group
        del table['age']

        table = table.groupby(['age_group', 'year'])[grouped].sum().reset_index()
        table['country'] = country

        return table

    def groupby_noage_add_ctr(self, table, country):
        table['country'] = country
        table = table.groupby(['country', 'year'])['female', 'male'].sum().reset_index()

        return table

    def pop_table_by_AG(self):
        total_population_AG = pd.DataFrame(columns=['country', 'year', 'age_group', 'female', 'male'])

        for country in self._countries:
            population = self._population.loc[self._population.country == country].copy()
            population = InputsLoader.group_age_groups_add_ctr(self, population, country)
            total_population_AG = total_population_AG.append(population, ignore_index=True)
        total_population_AG.reset_index()

        return total_population_AG

    def times_population(self, table, country, year_range, male='male', female='female'):

        table = table.loc[table['year'].isin(year_range)].reset_index()
        pop_ctr = self._population.loc[(self._population.country == country) & self._population['year'].isin(year_range)].reset_index()
        table[male] = np.prod([table[male], pop_ctr['male']], axis=0)
        table[female] = np.prod([table[female], pop_ctr['female']], axis=0)

        return table

    def in_rate_AG(self, table, year_range, male='male', female='female'):

        pop_AG = self._popAG.loc[self._popAG['year'].isin(year_range)].copy().reset_index()
        table[female] = np.divide(table[female], pop_AG['female'])
        table[male] = np.divide(table[male], pop_AG['male'])
        return table

    def in_rate_all(self, table, year_range, male='male', female='female', all='all'):

        pop = self._tot_pop.loc[self._tot_pop['year'].isin(year_range)].copy().reset_index()
        table[all] = np.divide(table[male] + table[female], pop['male'] + pop['female'])
        table[female] = np.divide(table[female], pop['female'])
        table[male] = np.divide(table[male], pop['male'])
        return table

    def pop_AG_aggregate(self):
        population_AG_aggregate = self._popAG.groupby(['country', 'age_group'])['female', 'male'].mean().reset_index()
        return population_AG_aggregate

    def employment_table(self):
        total_empl = pd.DataFrame(columns=['country', 'year', 'age_group', 'female', 'male'])

        for country in self._countries:
            employment = InputsLoader.get_table_from_db(self, country, 'IndirectCost_EmploymentRate')
            employment = InputsLoader.times_population(self, employment, country, YEARS2)

            employment = InputsLoader.group_age_groups_add_ctr(self, employment, country)
            total_empl = total_empl.append(employment, ignore_index=True)

        total_empl = InputsLoader.in_rate_AG(self, total_empl.reset_index(), YEARS2)

        return total_empl

    def disease_table(self, disease):
        incidence_final = pd.DataFrame(columns=['country', 'year', 'female', 'male'])
        prevalence_final = pd.DataFrame(columns=['country', 'year', 'female', 'male'])

        for country in self._countries:
            disease_table = InputsLoader.get_table_from_db(self, country, 'Disease_' + disease)

            incidence = disease_table[['year', 'age', 'female_incidence', 'male_incidence']
                        ].rename(columns={'female_incidence': 'female', 'male_incidence': 'male'})
            incidence = InputsLoader.times_population(self, incidence, country, YEARS_DISEASE)
            incidence_final = incidence_final.append(InputsLoader.groupby_noage_add_ctr(self, incidence, country),
                                                     ignore_index=True)

            prevalence = disease_table[['year', 'age', 'female_prevalence', 'male_prevalence']
                                        ].rename(columns={'female_prevalence': 'female', 'male_prevalence': 'male'})
            prevalence = InputsLoader.times_population(self, prevalence, country, YEARS_DISEASE)
            prevalence_final = prevalence_final.append(InputsLoader.groupby_noage_add_ctr(self, prevalence, country),
                                                       ignore_index=True)
        prevalence_final.reset_index()
        incidence_final.reset_index()

        pop = self._tot_pop.loc[self._tot_pop['year'].isin(YEARS_DISEASE)].copy().reset_index()
        prevalence_final['all'] = np.divide(prevalence_final['male'] + prevalence_final['female'], pop['male'] + pop['female'])
        incidence_final['all'] = incidence_final['male'] + incidence_final['female']

        prevalence_final = InputsLoader.in_rate_all(self, prevalence_final, YEARS_DISEASE, male='male', female='female')

        return incidence_final, prevalence_final

    def cancer_incidence(self):
        cancer_incidence = pd.DataFrame(columns=['country', 'year', 'female', 'male'])
        for country in self._countries:
            cancers = pd.DataFrame(columns=['country', 'year', 'female', 'male'])

            cancers['year'] = range(2005, 2051)
            cancers['country'] = country
            cancers['female'] = 0
            cancers['male'] = 0

            for cancer in self._cancers:
                cancer_table = InputsLoader.get_table_from_db(self, country, 'Disease_' + cancer
                                           )[['year', 'age', 'female_incidence', 'male_incidence']
                                           ].rename(columns={'female_incidence': 'female', 'male_incidence': 'male'})

                cancer_table = InputsLoader.groupby_noage_add_ctr(self, InputsLoader.times_population(self,
                                                                        cancer_table, country, YEARS_DISEASE), country)

                cancers['male'] += cancer_table['male']
                cancers['female'] += cancer_table['female']

            cancer_incidence = cancer_incidence.append(cancers, ignore_index=True)

        cancer_incidence.reset_index()
        cancer_incidence['all'] = cancer_incidence['male'] + cancer_incidence['female']

        return cancer_incidence

    def obesity_overweight(self):
        obesity_overweight = pd.DataFrame(columns=['country', 'year', 'age_group', 'female_overweight', 'female_obesity', 'male_overweight', 'male_obesity'])
        for country in self._countries:
            BMI_data = InputsLoader.get_table_from_db(self, country, 'RiskFactor_BMI')
            BMI_data = BMI_data[['age', 'year','female_d25_30', 'female_d30_35', 'female_d35_40', 'female_d40_',
                                               'male_d25_30', 'male_d30_35', 'male_d35_40', 'male_d40_']].loc[BMI_data['year'].isin(YEARS4)]

            for gender in 'female male'.split():
                pop_ctr = self._population.loc[(self._population.country == country) & self._population['year'].isin(YEARS4)]
                BMI_data[gender+'_overweight'] = np.prod([BMI_data[gender+'_d25_30'], pop_ctr[gender]], axis=0)
                BMI_data[gender+'_obesity'] = np.prod([(BMI_data[gender+'_d30_35'] + BMI_data[gender+'_d35_40'] + BMI_data[gender+'_d40_']), pop_ctr[gender]], axis=0)

            BMI_data = InputsLoader.group_age_groups_add_ctr(self, BMI_data, country,
                                    grouped='male_overweight male_obesity female_overweight female_obesity'.split())

            obesity_overweight = obesity_overweight.append(BMI_data, ignore_index=True)

        obesity_overweight = InputsLoader.in_rate_AG(self, obesity_overweight, YEARS4, male='male_overweight', female='female_overweight')
        obesity_overweight = InputsLoader.in_rate_AG(self, obesity_overweight, YEARS4, male='male_obesity', female='female_obesity')

        obesity_overweight = obesity_overweight.groupby(['country', 'age_group'])['female_overweight', 'female_obesity',
                                                                'male_overweight', 'male_obesity'].mean().reset_index()

        return obesity_overweight

    def alcohol_prevalence(self):
        alcohol_prevalence = pd.DataFrame(columns=['country', 'year', 'female_current', 'male_current', 'female_binge', 'male_binge'])

        for country in self._countries:
            alcohol = InputsLoader.get_table_from_db(self, country, 'RiskFactor_Alcohol')[['year', 'age',
                                                           'female_current', 'male_current', 'female_binge', 'male_binge']]

            alcohol = InputsLoader.times_population(self, alcohol, country, YEARS5, male='male_binge', female='female_binge')
            alcohol = InputsLoader.times_population(self, alcohol, country, YEARS5, male='male_current', female='female_current')

            alcohol = InputsLoader.group_age_groups_add_ctr(self, alcohol, country,
                                   grouped='male_binge female_binge male_current female_current'.split())

            alcohol_prevalence = alcohol_prevalence.append(alcohol, ignore_index=True)

        alcohol_prevalence = InputsLoader.in_rate_AG(self, alcohol_prevalence, YEARS5, male='male_current', female='female_current')
        alcohol_prevalence = InputsLoader.in_rate_AG(self, alcohol_prevalence, YEARS5, male='male_binge', female='female_binge')

        alcohol_prevalence = alcohol_prevalence.groupby(['country', 'age_group'])['female_current', 'male_current', 'female_binge', 'male_binge'].mean().reset_index()

        return alcohol_prevalence

    def health_expenditure(self, pc=False):
        inputs_HE = pd.read_csv(self._HE + 'SHAexport.csv')
        inputs_HE = inputs_HE.loc[inputs_HE['TIME'].isin(range(2005, 2017))]

        inputs_HE.loc[inputs_HE['PowerCode Code'] != 0, 'Value'] = inputs_HE.Value * 10**(inputs_HE['PowerCode Code'])

        curative = inputs_HE.loc[inputs_HE.HC == 'HC1HC2'].set_index(['LOCATION', 'TIME', 'MEASURE'], drop=False)
        preventive = inputs_HE.loc[inputs_HE.HC == 'HC6'].set_index(['LOCATION', 'TIME', 'MEASURE'], drop=False)
        goods = inputs_HE.loc[inputs_HE.HC == 'HC5'].set_index(['LOCATION', 'TIME', 'MEASURE'], drop=False)

        HE_data = pd.DataFrame(columns=['country', 'year', 'pc', 'all'])

        HE_data['country'] = curative['LOCATION']
        HE_data['year'] = curative['TIME']
        HE_data['pc'] = (curative.MEASURE=='UNPPER')*1
        HE_data['all'] = curative['Value'] + preventive['Value'] + goods['Value']
        HE_data['preventive'] = preventive['Value']
        HE_data['curative'] = curative['Value']
        HE_data['goods'] = goods['Value']

        HE_data.loc[(curative.Value == np.NaN)|(preventive.Value == np.NaN)|(goods.Value == np.NaN), 'all'] = np.NaN

        if pc:
            HE_data = HE_data.loc[HE_data.pc == 1].reset_index()
        else:
            HE_data = HE_data.loc[HE_data.pc == 0].reset_index()

        HE_data = self._converter.euro_converter(HE_data)

        return HE_data