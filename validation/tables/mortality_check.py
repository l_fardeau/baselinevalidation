import pandas as pd

from . import InputsLoader


IHME_DISEASES = ['Diabetes', 'IHD_MI', 'Ischemic_Stroke', 'Hemorrhagic_Stroke', 'copd', 'Cirrhosis', 'Depression',
                 'Dementia', 'Lower_respiratory_infections', 'Atrial_fibrillation', 'Rheumatoid_Arthritis', 'Gout',
                 'Rheumatoid_Arthritis', ] #'CKD',

IARC_DISEASES = ['BreastCancer', 'ColorectumCancer', 'EsophagealCancer', 'LungCancer', 'NasopharynxCancer', 'StomachCancer']


class MortalityCheck(InputsLoader):
    def __init__(self, inputs_path, countries):
        self._path = inputs_path
        self._countries = countries
        self._loader = InputsLoader(inputs_path, countries)

    def mortality_table(self):
        mortality = pd.DataFrame(columns='country year age female male'.split())
        for country in self._countries:
            mortality_ctr = self._loader.get_table_from_db(country, 'DeathProbability')
            mortality_ctr = mortality_ctr.loc[mortality_ctr.age == 60]
            mortality_ctr['country'] = country
            mortality = mortality.append(mortality_ctr, ignore_index=True)
        return mortality

    def IHD_death_table(self):
        IHD_mort_table = pd.DataFrame(columns='country year age female_fatality male_fatality'.split())
        for country in self._countries:
            IHD_mort_table_ctr = self._loader.get_table_from_db(country, 'Disease_IHD_MI')[['year', 'age', 'male_fatality', 'female_fatality']]
            IHD_mort_table_ctr = IHD_mort_table_ctr.loc[IHD_mort_table_ctr.age == 60]
            IHD_mort_table_ctr['country'] = country
            IHD_mort_table = IHD_mort_table.append(IHD_mort_table_ctr, ignore_index=True)
        return IHD_mort_table

    def diseases_mortality_IHME(self):
        mortality = pd.DataFrame(columns='country year age female_mortality male_mortality'.split())
        for dd in IHME_DISEASES:
            disease = pd.DataFrame(columns='country year age female_mortality male_mortality'.split())
            for country in self._countries:
                disease_ctr = self._loader.get_table_from_db(country, 'Disease_' + dd)[['year', 'age', 'male_prevalence', 'female_prevalence','male_fatality', 'female_fatality']]
                disease_ctr['female_mortality'] = disease_ctr.female_prevalence * disease_ctr.female_fatality * 1e-2
                disease_ctr['male_mortality'] = disease_ctr.male_prevalence * disease_ctr.male_fatality * 1e-2
                disease_ctr = disease_ctr[['year', 'age', 'female_mortality', 'male_mortality']].loc[disease_ctr.age==60]
                disease_ctr['country'] = country
                disease = disease.append(disease_ctr, ignore_index=True)
            mortality = mortality.append(disease, ignore_index=True)
        mortality = mortality.groupby(['country', 'year', 'age'])['male_mortality', 'female_mortality'].sum().reset_index()
        return mortality

    def cancers_mortality_IARC(self):
        mortality = pd.DataFrame(columns='country year age female_mortality male_mortality'.split())
        for dd in IARC_DISEASES:
            disease = pd.DataFrame(columns='country year age female_mortality male_mortality'.split())
            for country in self._countries:
                disease_ctr = self._loader.get_table_from_db(country, 'Disease_' + dd)[['year', 'age', 'male_mortality_rate', 'female_mortality_rate']
                                              ].rename(columns={'male_mortality_rate': 'male_mortality', 'female_mortality_rate': 'female_mortality'})
                disease_ctr['country'] = country
                disease = disease.append(disease_ctr, ignore_index=True)
            mortality = mortality.append(disease, ignore_index=True)
        mortality = mortality.groupby(['country', 'year', 'age'])['male_mortality', 'female_mortality'].sum().reset_index()
        return mortality

    def diseases_mortality(self):
        mortality = MortalityCheck.diseases_mortality_IHME(self)
        cancers_mortality = MortalityCheck.cancers_mortality_IARC(self)
        mortality = mortality.append(cancers_mortality, ignore_index=True)
        mortality = mortality.groupby(['country', 'year', 'age'])['male_mortality', 'female_mortality'].sum().reset_index()
        return mortality

    def final_table(self):
        diseases_mortality = MortalityCheck.diseases_mortality(self)
        mortality_table = MortalityCheck.mortality_table(self)
        mortality_table['female_residual_mortality'] = mortality_table.female - diseases_mortality.female_mortality
        mortality_table['male_residual_mortality'] = mortality_table.male - diseases_mortality.male_mortality
        return mortality_table, diseases_mortality